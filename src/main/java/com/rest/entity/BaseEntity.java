package com.rest.entity;

import java.io.Serializable;

public abstract class BaseEntity<K extends Serializable>{
    public abstract K getId();
}

