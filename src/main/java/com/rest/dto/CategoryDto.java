package com.rest.dto;

public class CategoryDto {

	private Integer categoryId;
	private String categoryName;
	
	
	
	public CategoryDto() {
		super();
	}
	
	

	public CategoryDto(String categoryName) {
		super();
		this.categoryName = categoryName;
	}



	public CategoryDto(Integer categoryId, String categoryName) {
		super();
		this.categoryId = categoryId;
		this.categoryName = categoryName;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Override
	public String toString() {
		return "CategoryDto [categoryId=" + categoryId + ", categoryName=" + categoryName + "]";
	}
	
	
	
	
	
	
	
}
