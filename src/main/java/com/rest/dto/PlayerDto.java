package com.rest.dto;

public class PlayerDto {
	
	private Integer playerId;
	private String playerName;
	private String playerProfile;
	
	
	
	public PlayerDto(Integer playerId, String playerName, String playerProfile) {
		super();
		this.playerId = playerId;
		this.playerName = playerName;
		this.playerProfile = playerProfile;
	}
	
	
	public PlayerDto(String playerName, String playerProfile) {
		super();
		this.playerName = playerName;
		this.playerProfile = playerProfile;
	}





	public PlayerDto() {
		super();
	}



	public Integer getPlayerId() {
		return playerId;
	}
	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getPlayerProfile() {
		return playerProfile;
	}
	public void setPlayerProfile(String playerProfile) {
		this.playerProfile = playerProfile;
	}



	@Override
	public String toString() {
		return "PlayerDto [playerId=" + playerId + ", playerName=" + playerName + ", playerProfile=" + playerProfile
				+ "]";
	}
	
	
	
	

}
