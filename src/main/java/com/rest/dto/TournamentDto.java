package com.rest.dto;

import java.util.List;

public class TournamentDto {
	
	private Integer tournamentId;
	private String name;
	private String location;
	private List<String> category;
	
	
	
	public TournamentDto(Integer tournamentId, String name, String location) {
		super();
		this.tournamentId = tournamentId;
		this.name = name;
		this.location = location;
	}
	
	
	
	public TournamentDto() {
		super();
	}



	public Integer getTournamentId() {
		return tournamentId;
	}
	public void setTournamentId(Integer tournamentId) {
		this.tournamentId = tournamentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	public List<String> getCategory() {
		return category;
	}


	public void setCategory(List<String> category) {
		this.category = category;
	}



	@Override
	public String toString() {
		return "TournamentDto [tournamentId=" + tournamentId + ", name=" + name + ", location=" + location + "]";
	}
	
	

}
