package com.rest.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.dto.PlayerDto;
import com.rest.mapper.PlayerMapper;
import com.rest.service.PlayerService;

@RestController
@RequestMapping("/players")
public class PlayerController {
	
	private final PlayerService playerService;

	public PlayerController(PlayerService playerService) {
		this.playerService = playerService;
	}
	
	@GetMapping
	public ResponseEntity<List<PlayerDto>> getAllPlayers() {
		return ResponseEntity.ok(playerService.listPlayers());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<PlayerDto> getPlayerById(@PathVariable(name = "id") Integer playerId){
		return ResponseEntity.ok(PlayerMapper
				.toDto(playerService.findById(playerId)));
	}
	
	@PostMapping
	public ResponseEntity<PlayerDto> addPlayer(@RequestBody PlayerDto playerDto){
		return ResponseEntity.ok(playerService.addPlayer(playerDto));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<PlayerDto> updatePlayer(@PathVariable Integer id,@RequestBody PlayerDto playerDto){
		return ResponseEntity.ok(playerService.updatePlayer(id, playerDto));
	}
	

}
