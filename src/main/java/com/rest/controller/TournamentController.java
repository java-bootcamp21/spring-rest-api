package com.rest.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rest.dto.PlayerDto;
import com.rest.dto.TournamentDto;
import com.rest.mapper.PlayerMapper;
import com.rest.mapper.TournamentMapper;
import com.rest.service.CategoryService;
import com.rest.service.TournamentService;

@RestController
@RequestMapping("/tournament")
public class TournamentController {
	
	private final TournamentService tournamentService;
	private final CategoryService categoryService;

	
	
	public TournamentController(TournamentService tournamentService, CategoryService categoryService) {
		super();
		this.tournamentService = tournamentService;
		this.categoryService = categoryService;
	}

	@GetMapping
	public ResponseEntity<List<TournamentDto>> getTournaments() {
		return ResponseEntity.ok(tournamentService.listTournament());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TournamentDto> getTournamentById(@PathVariable(name = "id") Integer id){
		return ResponseEntity.ok(TournamentMapper
				.toDto(tournamentService.findTournament(id)));
	}
	
	@PostMapping
	public ResponseEntity<TournamentDto> addTournament(@RequestBody TournamentDto tournamentDto){
		return ResponseEntity.ok(tournamentService.addTournament(tournamentDto));
	}
	
	@GetMapping("/{id}/category")
	public ResponseEntity<TournamentDto> addTournamentCat(@PathVariable Integer id, @RequestParam(name = "categoryId") Integer catId){
		var category = categoryService.findCategoryById(catId);
		return ResponseEntity.ok(tournamentService.addTournamentCategory(id, category));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteId(@PathVariable Integer id){
		tournamentService.deleteTournamet(id);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	

}
