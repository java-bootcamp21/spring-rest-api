package com.rest.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.rest.exception.GeneriExceptionResponse;
import com.rest.exception.GenericException;

import jakarta.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GenericExceptionHandler {
	
	@ExceptionHandler
	public ResponseEntity<GeneriExceptionResponse> handleGenericException(GenericException exp, HttpServletRequest req){
		var response = new GeneriExceptionResponse(HttpStatus.BAD_REQUEST.value(), req.getRequestURI(), exp.getMessage());
		return new ResponseEntity(response,HttpStatus.BAD_REQUEST);
	
	}
	
	

}
