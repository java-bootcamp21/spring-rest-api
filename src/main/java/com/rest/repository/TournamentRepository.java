package com.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.entity.Tournament;

@Repository
public interface TournamentRepository extends JpaRepository<Tournament,Integer>{

	List<Tournament> findAllByCategoriesIdAndCategoriesName(Integer categoryId,String categoryName);

}
