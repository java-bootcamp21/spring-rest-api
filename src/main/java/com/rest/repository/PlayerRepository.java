package com.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.entity.Player;
import java.util.List;


@Repository
public interface PlayerRepository extends JpaRepository<Player, Integer>{
	
}
