package com.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.entity.PlayerProfile;

@Repository
public interface PlayerProfileRepository extends JpaRepository<PlayerProfile,Integer> {



}
