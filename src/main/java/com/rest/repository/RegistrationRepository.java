package com.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.entity.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration,Integer> {
	
	Registration findFirstByIdAndPlayer_id(Integer id, Integer playerId);

}
