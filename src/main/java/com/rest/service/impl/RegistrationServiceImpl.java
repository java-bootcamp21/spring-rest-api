package com.rest.service.impl;

import org.springframework.stereotype.Service;

import com.rest.dto.RegistrationDto;
import com.rest.entity.Player;
import com.rest.entity.Registration;
import com.rest.entity.Tournament;
import com.rest.exception.GeneriExceptionResponse;
import com.rest.exception.GenericException;
import com.rest.repository.RegistrationRepository;
import com.rest.service.RegistrationService;

import static com.rest.mapper.RegistrationMapper.*;

@Service
public class RegistrationServiceImpl implements RegistrationService{

	private final RegistrationRepository regRepository;
	
	
	public RegistrationServiceImpl(RegistrationRepository regRepository) {
		super();
		this.regRepository = regRepository;
	}

	@Override
	public RegistrationDto addRegistration(Player player, Tournament tournament) {
		var reg = new Registration(tournament,player);
		return toDto(regRepository.save(reg));
	}

	@Override
	public void deleteRegistration(Integer id) {
		var registrationToDelete = findRegistrationById(id);
		regRepository.delete(registrationToDelete);
	}

	@Override
	public Registration findRegistrationById(Integer id) {
		return regRepository.findById(id)
				.orElseThrow(()-> new GenericException(String
						.format("Registration with id %s not found", id)));
	}

	
}
