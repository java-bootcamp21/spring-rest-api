package com.rest.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.rest.dto.CategoryDto;
import com.rest.entity.Category;
import com.rest.exception.GenericException;
import com.rest.repository.CategoryRepository;
import com.rest.service.CategoryService;

import static com.rest.mapper.CategoryMapper.*;

@Service
public class CategoryServiceImpl implements CategoryService{

	private final CategoryRepository categoryRepository;
	
	
	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		super();
		this.categoryRepository = categoryRepository;
	}

	@Override
	public CategoryDto addCategory(CategoryDto c) {
		var categoryToadd = categoryRepository.save(toEntity(c));
		return toDto(categoryToadd);
	}

	@Override
	public CategoryDto getCategory(Integer categoryId) {
		return categoryRepository.findById(categoryId)
				.map(c -> toDto(c))
				.orElseThrow(()-> new GenericException(String
						.format("Category with id %s not found", categoryId)));
	}

	@Override
	public List<CategoryDto> getCategories() {
		return categoryRepository.findAll().stream()
				.map(c -> toDto(c))
				.collect(Collectors.toList());
	}

	@Override
	public Category findCategoryById(Integer categoryId) {
		return categoryRepository.findById(categoryId)
				.orElseThrow(()-> new GenericException(String
						.format("Category with id %s not found", categoryId)));
	}

}
