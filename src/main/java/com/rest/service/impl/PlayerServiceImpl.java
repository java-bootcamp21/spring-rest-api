package com.rest.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.rest.dto.PlayerDto;
import com.rest.entity.Player;
import com.rest.entity.PlayerProfile;
import com.rest.exception.GenericException;
import com.rest.mapper.PlayerMapper;
import com.rest.repository.PlayerRepository;
import com.rest.service.PlayerService;

import static com.rest.mapper.PlayerMapper.*;

@Service
public class PlayerServiceImpl implements PlayerService{
	
	private final PlayerRepository playerRepository;
	
	public PlayerServiceImpl(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}

	@Override
	public PlayerDto addPlayer(PlayerDto player) {
		var playerToAdd = toEntity(player);
		return toDto(playerRepository.save(playerToAdd));
	}

	@Override
	public Player findById(Integer id) {
		return playerRepository.findById(id)
				.orElseThrow(()-> new GenericException(String
						.format("player with id %s not found", id)));
	}

	@Override
	public List<PlayerDto> listPlayers() {
		return playerRepository.findAll().stream()
				.map(p -> toDto(p))
				.collect(Collectors.toList());
	}

	@Override
	public void deletePlayer(Integer id) {
		playerRepository.findById(id)
				.ifPresentOrElse(p -> playerRepository.delete(p)
						, () -> new GenericException(String
								.format("cannot delete player! Player with id %s does not exist", id)));;
		
	}

	@Override
	public PlayerDto addPlayerProfile(Integer playerId, String profile) {
		var player = findById(playerId);
		player.setPlayerProfile(new PlayerProfile(profile));
		return toDto(playerRepository.save(player));
	}

	@Override
	public PlayerDto updatePlayer(Integer id,PlayerDto player) {
		var pl = findById(id);
		pl.setName(player.getPlayerName());
		return toDto(playerRepository.save(pl));
	}


}
