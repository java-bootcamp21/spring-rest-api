package com.rest.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.rest.dto.TournamentDto;
import com.rest.entity.Category;
import com.rest.entity.Tournament;
import com.rest.exception.GenericException;
import com.rest.repository.TournamentRepository;
import com.rest.service.TournamentService;

import static com.rest.mapper.TournamentMapper.*;

@Service
public class TournamentServiceImpl implements TournamentService{

	private final TournamentRepository tourRepository;

	public TournamentServiceImpl(TournamentRepository tourRepository) {
		super();
		this.tourRepository = tourRepository;
	}

	@Override
	public Tournament findTournament(Integer id) {
		return tourRepository.findById(id)
				.orElseThrow(()-> new GenericException(String
						.format("Tournament with id %s not found", id)));
	}

	@Override
	public TournamentDto addTournament(TournamentDto tournament) {
		var tournamentToAdd = toEntity(tournament);
		return toDto(tourRepository.save(tournamentToAdd));
	}

	@Override
	public List<TournamentDto> listTournament() {
		return tourRepository.findAll().stream()
				.map(t -> toDto(t))
				.collect(Collectors.toList());
	}

	@Override
	public TournamentDto addTournamentCategory(Integer tounamentId, Category category) {
		var t = findTournament(tounamentId);
	    t.getCategories().add(category);
		return toDto(tourRepository.save(t));
	}

	@Override
	public TournamentDto removeCategory(Integer tounamentId, Integer categoryId) {
		var t = findTournament(tounamentId);
		var updatedTournamentCategories = t.getCategories().stream()
				.filter(c -> c.getId()!=categoryId)
				.collect(Collectors.toList());
		t.setCategories(updatedTournamentCategories);
		return toDto(tourRepository.save(t));
	}

	@Override
	public void deleteTournamet(Integer tournamentId) {
		var toDelete = findTournament(tournamentId);
		tourRepository.delete(toDelete);
		
	}
}
