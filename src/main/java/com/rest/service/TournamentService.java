package com.rest.service;

import java.util.List;

import com.rest.dto.TournamentDto;
import com.rest.entity.Category;
import com.rest.entity.Tournament;

public interface TournamentService {
	
	Tournament findTournament(Integer id);
	TournamentDto addTournament(TournamentDto tournament);
	List<TournamentDto> listTournament();
	TournamentDto addTournamentCategory(Integer tounamentId,Category category);
	TournamentDto removeCategory(Integer tounamentId,Integer categoryId);
	void deleteTournamet(Integer tournamentId);
	
}
