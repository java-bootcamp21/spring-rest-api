package com.rest.service;

import com.rest.dto.RegistrationDto;
import com.rest.entity.Player;
import com.rest.entity.Tournament;
import com.rest.entity.Registration;

public interface RegistrationService {
	
	RegistrationDto addRegistration(Player player, Tournament tournament);
	Registration findRegistrationById(Integer id);
	void deleteRegistration(Integer id);

}
