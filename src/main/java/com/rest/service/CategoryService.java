package com.rest.service;

import java.util.List;

import com.rest.dto.CategoryDto;
import com.rest.entity.Category;

public interface CategoryService {
	
	Category findCategoryById(Integer categoryId);
	CategoryDto addCategory(CategoryDto c);
	CategoryDto getCategory(Integer categoryId);
	List<CategoryDto> getCategories();
}
