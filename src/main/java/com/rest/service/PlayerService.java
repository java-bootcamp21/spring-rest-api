package com.rest.service;

import java.util.List;

import com.rest.dto.PlayerDto;
import com.rest.entity.Player;

public interface PlayerService {
	
	PlayerDto addPlayer(PlayerDto player);
	PlayerDto updatePlayer(Integer id, PlayerDto player);
	Player findById(Integer id);
	List<PlayerDto> listPlayers();
	void deletePlayer(Integer id);
	PlayerDto addPlayerProfile(Integer playerId, String profile);
	
}
