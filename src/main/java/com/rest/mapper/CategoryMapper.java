package com.rest.mapper;

import com.rest.dto.CategoryDto;
import com.rest.entity.Category;

public class CategoryMapper {
	
	public static Category toEntity(CategoryDto c) {
		return new Category(c.getCategoryName());
	}
	
	public static CategoryDto toDto(Category c) {
		return new CategoryDto(c.getId(), c.getName());
	}
	

}
