package com.rest.mapper;

import com.rest.dto.PlayerDto;
import com.rest.entity.Player;
import com.rest.entity.PlayerProfile;

public class PlayerMapper {
	
	public static PlayerDto toDto(Player p) {
		String profile = p.getPlayerProfile()!=null?p.getPlayerProfile().getTwitter():null;
		return new PlayerDto(p.getId(), p.getName(), profile);
	}
	
	public static Player toEntity(PlayerDto p) {
		var player = new Player();
		player.setName(p.getPlayerName());
		player.setPlayerProfile(p.getPlayerProfile()!=null?new PlayerProfile(p.getPlayerProfile()):null);
		return player;
	}

}
