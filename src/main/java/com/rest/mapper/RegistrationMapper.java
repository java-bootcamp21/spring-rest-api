package com.rest.mapper;

import com.rest.dto.RegistrationDto;
import com.rest.entity.Registration;

public class RegistrationMapper {
	
	public static RegistrationDto toDto(Registration reg) {
		return new RegistrationDto(reg.getId(), reg.getPlayer().getId(),
						reg.getPlayer().getName(), 
						reg.getTournament().getId(), 
						reg.getTournament().getName());
	}

}
