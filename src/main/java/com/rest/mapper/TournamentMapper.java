package com.rest.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.rest.dto.TournamentDto;
import com.rest.entity.Tournament;

public class TournamentMapper {
	
	public static Tournament toEntity(TournamentDto t) {
		return new Tournament(t.getName(),t.getLocation());
	}
	
	public static TournamentDto toDto(Tournament t) {
		var tour = new TournamentDto(t.getId(), t.getName(),t.getLocation());
		List<String> categories = t.getCategories()==null?null:t.getCategories().stream()
				.map(ca -> ca.getName() ).collect(Collectors.toList());
		tour.setCategory(categories);
		return  tour;
	}

}
